#+TITLE: Moth Xmonad
#+AUTHOR: Timothy Bump
#+BABEL: :cache yes
#+PROPERTY: header-args :tangle yes
(require 'ob-haskell)
* Imports
[2018-07-24 Tue 01:06]
#+BEGIN_SRC haskell
import XMonad
import XMonad.Config.Desktop
#+END_SRC
* Main
[2018-07-24 Tue 01:06]
#+BEGIN_SRC haskell

main = xmonad desktopConfig
    { terminal    = "gnome-terminal"
    , modMask     = mod4Mask
    }

#+END_SRC
* Theme Variables
[2018-07-24 Tue 01:53]
#+BGEIN_SRC haskell :tangle no
-- -- Colors
mothzenfgp1     = "#FFFFEF"
mothzenfg       = "#DCDCCC"
mothzenfg1     = "#656555"
mothzenbg2     = "#000000"
mothzenbg1     = "#2B2B2B"
mothzenbg05    = "#383838"
mothzenbg       = "#3F3F3F"
mothzenbgp05    = "#494949"
mothzenbgp1     = "#4F4F4F"
mothzenbgp2     = "#5F5F5F"
mothzenbgp3     = "#6F6F6F"
mothzenredp2    = "#ECB3B3"
mothzenredp1    = "#DCA3A3"
mothzenred      = "#CC9393"
mothzenred1    = "#BC8383"
mothzenred2    = "#AC7373"
mothzenred3    = "#9C6363"
mothzenred4    = "#8C5353"
mothzenred5    = "#7C4343"
mothzenred6    = "#6C3333"
mothzenorange   = "#DFAF8F"
mothzenyellow   = "#F0DFAF"
mothzenyellow1 = "#E0CF9F"
mothzenyellow2 = "#D0BF8F"
mothzengreen5  = "#2F4F2F"
mothzengreen4  = "#3F5F3F"
mothzengreen3  = "#4F6F4F"
mothzengreen2  = "#5F7F5F"
mothzengreen1  = "#6F8F6F"
mothzengreen    = "#7F9F7F"
mothzengreenp1  = "#8FB28F"
mothzengreenp2  = "#9FC59F"
mothzengreenp3  = "#AFD8AF"
mothzengreenp4  = "#BFEBBF"
mothzencyan     = "#93E0E3"
mothzenbluep3   = "#BDE0F3"
mothzenbluep2   = "#ACE0E3"
mothzenbluep1   = "#94BFF3"
mothzenblue     = "#8CD0D3"
mothzenblue1   = "#7CB8BB"
mothzenblue2   = "#6CA0A3"
mothzenblue3   = "#5C888B"
mothzenblue4   = "#4C7073"
mothzenblue5   = "#366060"
mothzenmagenta  = "#DC8CC3"

-- -- sizes
gap    = 8
topbar = 8
border = 0
prompt = 20

myNormalBorderColor     = "#000000"
myFocusedBorderColor    = active

active      = mothzenbg1
activeWarn  = mothzenmagenta
inactive    = mothzenbgp1
focusColor  = mothzenyellow
unfocusColor = mothzenbgp1

-- fonts
myFont      = "-nerdypepper-curie-normal-normal-normal-*-12-*-*-*-c-60-iso10646-1"
myBigFont   = "-*-terminus-medium-*-*-*-*-240-*-*-*-*-*-*"
myWideFont  = "xft:Dejavu Serif:style=Regular:pixelsize=180:hinting=true"

#+END_SRC
